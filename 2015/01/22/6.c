#include <stdio.h>
#include <float.h>
void print_star(void);
int main(void)
{
    double i;
    float j;


    i = 1.0 / 3.0;
    j = 1.0 / 3.0;

    printf("i = %.4f\n",i);
    printf("i = %.12f\n",i);
    printf("i = %.16f\n",i);

    print_star();

    printf("j = %.4f\n",j);
    printf("j = %.12f\n",j);
    printf("j = %.16f\n",j);

    print_star();

    printf("%f\n",FLT_DIG);
    printf("%f\n",DBL_DIG);
    return 0;
}

void print_star(void)
{
    int k;
    for(k = 0;k < 40;k++)
        printf("%s","*");
    printf("\n");
}
