#include <stdio.h>
void one(void);
void two(void);

int main(void)
{
    printf("starting now:\n");
    one();
    two();
    printf("three\n");
    printf("done!\n");

    return 0;
}

void one(void)
{
    printf("one\n");
}

void two(void)
{
    printf("two\n");
}
