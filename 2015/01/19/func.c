#include <stdio.h>
void printf_good(void);
void printf_deny(void);
int main(void)
{
    printf("For he's jolly good fellow!\n");
    printf_good();
    printf_deny();

    return 0;
}

void printf_good(void)
{
    printf("For he's jolly good fellow!\n");
}

void printf_deny(void)
{
    printf("which nobody can deny!");
}
