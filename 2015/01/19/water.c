#include <stdio.h>
#define WEIGHT 3.0E-23
#define WATER_WEIGHT 950
int main(void)
{
    int num;

    printf("How much water?\n");
    scanf("%d",&num);
    printf("%f or %e\n",num * WATER_WEIGHT / WEIGHT);

    return 0;
}
