#include <stdio.h>
#include <string.h>
int main(void)
{
    const int NUM = 100;
    char name[NUM];

    printf("What is your name?\n");
    scanf("%s",&name);

    printf("your name is \"%s\"\n",name);
    printf("\"%20s\"\n",name);
    printf("\"%-20s\"\n",name);
    /*注意动态指定输出字符宽度的方法，用*占位符莱代替输出宽度*/
    printf("\"%-*s\"\n",strlen(name)+33,name);
    return 0;

}
