#include <stdio.h>
int main(void)
{
    int i,j;
    const int NUM = 9;
    for(i=NUM;i>0;i--)
    {
        for(j=i;j<=NUM;j++)
            printf("%d * %d = %d ",i,j,i*j);
        printf("\n");
    }

    return 0;
}
